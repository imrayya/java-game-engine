package ui.graphic;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Replaced with ImageMap
 *
 * @author Imray
 * @see ImageMap
 * @deprecated
 */
@Deprecated
public class ImageHolder {

    private final static ArrayList<BufferedImage> IMAGES = new ArrayList<>();

    /**
     * Gets the number of images loaded
     *
     * @return The number of images
     */
    public static int getSize() {
        return IMAGES.size();
    }

    /**
     * Gets a specific image based on the index
     *
     * @param index Gets a specific image
     * @return The required image
     */
    public static BufferedImage getImage(int index) {
        if (index > getSize()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return IMAGES.get(index);
    }

    /**
     * Replaces a image with a new image
     *
     * @param index What image to replace
     * @param image The image to replace it
     */
    public static void replace(int index, BufferedImage image) {
        if (index > getSize()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        IMAGES.set(index, image);
    }

    /**
     * Adds a new image to the holder
     *
     * @param i The new image to be inputted
     * @return The index of the image
     */
    public static int addImage(BufferedImage i) {
        if (!IMAGES.contains(i)) {
            IMAGES.add(i);
        }
        return IMAGES.indexOf(i);
    }

    /**
     * Adds a series of images to the holder
     *
     * @param i The series of image
     * @return The indexes of the images
     */
    public static int[] addImage(BufferedImage[] i) {
        int[] index = new int[i.length];
        int counter = 0;
        for (BufferedImage i1 : i) {
            index[counter] = addImage(i1);
            counter++;
        }
        return index;
    }

}
