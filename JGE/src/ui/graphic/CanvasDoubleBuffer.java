package ui.graphic;

import global.events.KeyListner;
import global.events.MouseListener;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import global.setting.GraphicsSetting;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.util.concurrent.PriorityBlockingQueue;
import ui.graphic.Renderable.RenderableComparable;

/**
 * The canvas to draw on
 *
 * @author Imray
 */
public class CanvasDoubleBuffer extends JFrame {

    private final PriorityBlockingQueue<Renderable> IMAGES = new PriorityBlockingQueue<>(1, new RenderableComparable());

    protected int frameCounter = -1;

    public CanvasDoubleBuffer() {
        this.setSize(new Dimension(
                GraphicsSetting.getWidth(), GraphicsSetting.getWidth()));
        setDefaultCloseOperation(3);
        setUndecorated(true);
        setVisible(true);
        createBufferStrategy(2);

        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                KeyListner.getInstance().addItem(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {
                KeyListner.getInstance().removeItem(e);
            }

        });
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                MouseListener.getInstance().addItem(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                MouseListener.getInstance().addItem(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                MouseListener.getInstance().addItem(e);
            }

        });
        setLocationRelativeTo(null);
    }

    protected void addImages(Renderable image) {
        IMAGES.add(image);
    }

    protected void remove(Renderable image) {
        IMAGES.remove(image);
    }

    BufferedImage Scene;

    public void getScene() {
        BufferStrategy bi = this.getBufferStrategy();
        Graphics g = bi.getDrawGraphics();
//        g.fillRect(0, 0, 1000, 1000);//this is slowest part of this rendering thingy
        PriorityBlockingQueue<Renderable> a = new PriorityBlockingQueue<>(1, new RenderableComparable());
        a.addAll(IMAGES);
        a.stream().filter(Renderable::isVisible).filter((Renderable image) -> image.getImage() != null).
                forEach((Renderable image) -> {
                    g.drawImage(image.getImage(),
                            (int) image.getLocation().getX(),
                            (int) image.getLocation().getY(),
                            null);
                });
        if (frameCounter != -1) {
            g.setColor(Color.white);
            g.drawString(String.valueOf(frameCounter), 900, 900);
        }
        g.dispose();
        bi.show();
        Toolkit.getDefaultToolkit().sync();
    }
}
