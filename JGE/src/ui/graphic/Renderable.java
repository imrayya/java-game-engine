package ui.graphic;

import helper.Logger;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Comparator;

/**
 * What the UI renders
 *
 * @author Kareem
 */
public class Renderable implements Comparable<Renderable>, Cloneable {

    private Point2D LOCATION;
    private String index = "";
    private String[] indexs;
    private boolean moving = false;
    private boolean visible = true;
    private int priority = 0;
    protected int counter = 0;
    protected double resizeFactor = 1;

    /**
     * Set everything to null
     */
    public Renderable() {
        this.LOCATION = null;
        this.visible = false;
    }

    /**
     * Initial values for the object
     *
     * @param i The initial image to use
     * @param location The initial placement of the image
     */
    public Renderable(String i, Point2D location) {
        moving = false;
        index = i;
        this.LOCATION = location;
    }

    /**
     * Initial values for the object
     *
     * @param i The initial image to use
     * @param location The initial placement of the image
     */
    public Renderable(BufferedImage i, String key, Point2D location) {
        moving = false;
        index = key;
        this.LOCATION = location;
        addImage(i, key);
    }

    /**
     * Initial values for the object
     *
     * @param i The initial image to use
     * @param location The initial placement of the image
     */
    public Renderable(String[] i, Point2D location) {
        moving = true;
        indexs = i;
        this.LOCATION = location;
    }

    /**
     * Meant for a deep clone
     *
     * @param a Make a clone of 'a'
     */
    private Renderable(Renderable a) {
        LOCATION = new Point2D.Double(a.getX(), a.getY());
        index = a.index;
        indexs = Arrays.copyOf(a.indexs, a.indexs.length);
        moving = a.moving;
        visible = a.visible;
        priority = a.priority;
        counter = a.counter;

    }

    /**
     * Replaces a bufferedImage with another one
     *
     * @param i The image to replace with
     */
    public void replace(BufferedImage i) {
        ImageMap.getInstance().replace(i, index);
    }

    /**
     * Makes the animation longer. Assumes that this is a moving sprit
     *
     * @param a Has to be a factor of 2 (for nice results)
     */
    public void makeLonger(int a) {
        String[] tmp = new String[indexs.length * a];
        int counter = 0;
        int counter2 = 1;
        for (int i = 0; i < indexs.length; i++) {
            while (counter != a * counter2) {
                tmp[counter] = indexs[i];
                counter++;
            }
            counter2++;
        }
        indexs = tmp;
    }

    /**
     * Resizes the image
     *
     * @param factor The factor of the resize
     */
    public void resize(double factor) {
        resizeFactor = factor;
    }
    protected double rotation = 0;

    /**
     * Sets how much you want this image to rotated by
     *
     * @param rotation
     */
    public void rotate(double rotation) {
        this.rotation = rotation;
    }

    /**
     * If the image is visible or not
     *
     * @return If the image is visible or not
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Sets the object to be visible or not
     *
     * @param visible Whether you want the object to be visible or not
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * What priority of the rendering you want this object to have
     *
     * @param priority The requested priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Gets the priority of the object
     *
     * @return
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Set the location of the renderable object
     *
     * @param location The location of the object
     */
    public void setLocation(Point2D location) {
        if (LOCATION != null) {
            LOCATION.setLocation(location);
        } else {
            LOCATION = location;
        }
    }

    /**
     * Gets the location of the object
     *
     * @return The location of the object
     */
    public Point2D getLocation() {
        return LOCATION;
    }

    /**
     * Gets the current image
     *
     * @return The current image
     */
    public BufferedImage getImage() {
        if (moving) {
            if (counter >= indexs.length) {
                counter = 0;
            }
            BufferedImage tmp = ImageMap.getInstance().getImage(indexs[counter], rotation, resizeFactor);
            counter++;
            return tmp;
        }
        if ("".equals(index)) {
            Logger.LogError("Index is -1 in getImage", this);
            return null;
        }
        return ImageMap.getInstance().getImage(index, rotation, resizeFactor);
    }

    /**
     * Adds a new image
     *
     * @param image The image that wants to be added
     */
    public void addImage(String image) {
        if (moving) {
            String[] a = new String[indexs.length + 1];
            System.arraycopy(indexs, 0, a, 0, 10);
            a[a.length] = image;
        } else if (index == "") {
            index = image;
        } else {
            moving = true;
            indexs = new String[]{index, image};
        }
    }

    /**
     * Adds a new image
     *
     * @param image The image that wants to be added
     * @param key The key to call this string
     */
    public void addImage(BufferedImage image, String key) {
        if (moving) {
            ImageMap.getInstance().addImage(image, key);
            String[] a = new String[indexs.length + 1];
            System.arraycopy(indexs, 0, a, 0, 10);
            a[a.length] = key;
        } else if (index == "") {
            ImageMap.getInstance().addImage(image, key);

            index = key;
        } else {
            moving = true;
            ImageMap.getInstance().addImage(image, key);
            indexs = new String[]{index, key};
        }
    }

    /**
     * Comparable on priority
     *
     * @param o Comparable object
     * @return Which one is higher
     */
    @Override
    public int compareTo(Renderable o) {
        return priority - o.priority;
    }

    /**
     * Set the location
     *
     * @param x What the new x is
     */
    public void setX(int x) {
        LOCATION.setLocation(x, LOCATION.getY());
    }

    /**
     * Set the location
     *
     * @param y What the new y is
     */
    public void setY(int y) {
        LOCATION.setLocation(LOCATION.getX(), y);
    }

    /**
     * Gets the x location
     *
     * @return x-location
     */
    public int getX() {
        return (int) LOCATION.getX();
    }

    /**
     * Gets the y location
     *
     * @return y-location
     */
    public int getY() {
        return (int) LOCATION.getY();
    }

    /**
     * Makes a new clone of a renderable object
     *
     * @return A clone of this object
     */
    @Override
    public Renderable clone() {
        try {
            return (Renderable) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.LogError(ex.toString(), this);
        }
        return null;
    }

    /**
     * Does a deep copy of this object
     *
     * @return A deep copy
     */
    public Renderable copy() {
        return new Renderable(this);
    }

    public static class RenderableComparable implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            assert o1 instanceof Renderable && o2 instanceof Renderable;
            return ((Renderable) o1).compareTo((Renderable) o2);
        }

    }

}
