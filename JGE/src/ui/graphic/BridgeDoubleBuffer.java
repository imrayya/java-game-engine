package ui.graphic;

import helper.Logger;
import global.events.KeyListner;
import global.events.Observer;
import global.setting.GraphicsSetting;
import java.awt.event.KeyEvent;

/**
 * A bridge between the engine and the UI
 *
 * @author Kareem
 */
public class BridgeDoubleBuffer implements Runnable, Observer {

    /**
     * Boolean to say whether the System Out Println should been seen
     */
    private static final boolean DEBUG = true;
    /**
     * The canvas to draw on
     */
    private CanvasDoubleBuffer c;
    /**
     * The setting screen
     */
    private SettingScreen s;
    /**
     * Singleton holder of the bridge
     */
    private static final BridgeDoubleBuffer SINGLETON = new BridgeDoubleBuffer();

    /**
     * Private singleton creator
     *
     *
     */
    private BridgeDoubleBuffer() {
        super();
        c = new CanvasDoubleBuffer();
        c.setVisible(true);
        KeyListner.getInstance().addObserver(this);
    }

    /**
     * Gets the instance of the bridge
     *
     * @return Gets a instance of the bridge
     */
    public static BridgeDoubleBuffer getInstance() {
        return SINGLETON;
    }

    /**
     * Adds a new object to be rendered
     *
     * @param a A object to be rendered
     */
    public void add(Renderable a) {
        c.addImages(a);
    }

    /**
     * Tells the UI to render a new frame
     *
     * @param frameCounter What frame is currently being rendered. For debug
     * preposes
     */
    public void render(int frameCounter) {
        if (c != null) {
            if (DEBUG) {
                c.frameCounter = frameCounter;
            } else {
                c.frameCounter = -1;
            }
            c.getScene();
        }
    }

   

    /**
     * Set the UI to start running at a certain FPS ~ one time setup
     */
    @Override
    public void run() {
        int frameCounter = -1;
        int counter = 0;
        long start = System.currentTimeMillis();
        while (true) {
            if (System.currentTimeMillis() - start > 1000 / GraphicsSetting.FPS) {
                render(frameCounter++);
                start = System.currentTimeMillis();
            }

        }
    }

    /**
     * Removes a renderable object
     *
     * @param r The object to be removed
     */
    public void remove(Renderable r) {
        c.remove(r);
    }

    /**
     * Open the main UI again after the setting screen.
     * <p>
     * To be replaced with something better
     *
     * @deprecated
     */
    protected void showGameAgain() {
        System.out.println("ello");
        s.dispose();
        s = null;
        c.setVisible(true);
    }

    @Override
    public void update() {
        Integer[] check = KeyListner.getInstance().getItems();

        for (Integer check1 : check) {
            Logger.LogAny("KeyPress", KeyEvent.getKeyText(check1));
            if (check1 == KeyEvent.VK_ESCAPE) {
//                Logger.exit();
                if (s == null) {
                    c.setVisible(false);
                    s = new SettingScreen();
                    KeyListner.getInstance().clear();
                    s.setVisible(true);
                }
            }
        }
    }

    @Override
    public void update(String args) {
        Logger.LogError("Not supported yet.", this);
    }
}
