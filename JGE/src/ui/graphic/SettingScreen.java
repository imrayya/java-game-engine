package ui.graphic;

import helper.Logger;
import global.setting.GraphicsSetting;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A simple setting screen. Does nothing so far
 *
 * @author Kareem
 */
public class SettingScreen extends JFrame {

    /**
     * Creates the setting screen
     *
     * @throws HeadlessException I swear this isn't me
     */
    public SettingScreen() throws HeadlessException {
        super("Setting Screen");
        this.setSize(new Dimension(800, 600));
        setDefaultCloseOperation(3);
        setUndecorated(false);
        addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                }
            }
        });
        this.setLayout(new GridLayout(3, 1));
        JPanel Button = new JPanel();
        JButton exit = new JButton("Exit");
        exit.addActionListener((ActionEvent e) -> {
            Logger.printOther();
            Logger.exit();
        });
        JButton playAgain = new JButton("Back to Game");
        playAgain.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               BridgeDoubleBuffer.getInstance().showGameAgain();
            }
        });
        Button.add(exit);
        Button.add(playAgain);

        this.add(Button);

        JPanel resoultion = new JPanel();
        JPanel widthPanel = new JPanel();
        JLabel widthLabel = new JLabel("Screen Width: ");
        JTextField width = new JTextField(String.valueOf(GraphicsSetting.getWidth()));
        widthPanel.add(widthLabel);
        widthPanel.add(width);

        JPanel heightPanel = new JPanel();
        JLabel heightLabel = new JLabel("Screen Height: ");
        JTextField height = new JTextField(String.valueOf(GraphicsSetting.getHeight()));

        heightPanel.add(heightLabel);
        heightPanel.add(height);
        resoultion.add(widthPanel);
        resoultion.add(heightPanel);
        this.add(resoultion);
        setLocationRelativeTo(null);
    }

}
