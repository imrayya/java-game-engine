/*
 * Copyright (C) 2016 Imray
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ui.sound;

import helper.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import org.apache.commons.io.filefilter.FileFilterUtils;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 * Contains outline of a music player to be used internally. Also contains the
 * .mp3 player as well as the .wav player
 *
 * @author Kareem
 */
public abstract class MusicPlayer implements MusicPlayerInterface {

    protected File sound;

    @Override
    public final void load(File sound) throws IllegalArgumentException {
        if (FileFilterUtils.suffixFileFilter(".mp3").accept(sound)) {
            this.sound = sound;
        } else {
            throw new IllegalArgumentException("Not a mp3");
        }
    }

    /**
     * Plays an MP3 file
     */
    public static class MP3 extends MusicPlayer {

        public MP3() {
        }

        public MP3(File sound) throws IllegalArgumentException {
            load(sound);
        }

        @Override
        public void play() {
            Player p = null;
            try {
                p = new Player(new FileInputStream(sound));
            } catch (JavaLayerException ex) {
                Logger.LogError(ex);
            } catch (FileNotFoundException ex) {
                Logger.LogError(ex);
            }
            try {
                p.play();
            } catch (JavaLayerException ex) {
                Logger.LogError(ex);
            }
        }

        @Override
        public void stop() {
            Logger.LogError("Not supported yet.", this);
        }

        @Override
        public void pause() {
            Logger.LogError("Not supported yet.", this);
        }

    }

    public static class Wav extends MusicPlayer {

        @Override
        public void play() {
            InputStream in = null;
            try {
                in = new FileInputStream(sound);
            } catch (FileNotFoundException ex) {
                Logger.LogError(ex);
            }

            // create an audiostream from the inputstream
            AudioStream audioStream = null;
            try {
                audioStream = new AudioStream(in);
            } catch (IOException ex) {
                Logger.LogError(ex);
            }

            // play the audio clip with the audioplayer class
            AudioPlayer.player.start(audioStream);
        }

        @Override
        public void stop() {
            System.out.println("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void pause() {
            System.out.println("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }
}
