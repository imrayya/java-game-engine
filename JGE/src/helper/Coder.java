/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Base64;

/**
 * Encodes the objects to a binary array (base64) or Decodes byte arrays
 * (base64) to an object
 *
 * @author Kareem
 */
public class Coder {

    /**
     *
     * @param object
     * @return
     */
    public static byte[] getEncoded(Object object) {
        Base64 base64 = new Base64();
        try {
            return (byte[]) base64.encode(object);
        } catch (EncoderException ex) {
            Logger.LogError(ex);
        } catch (Exception ex) {
            Logger.LogError(ex);
        }

        return null;
    }

    /**
     *
     * @param object
     * @return
     */
    public static Object decode(byte[] object) {
        Base64 base64 = new Base64();
        object = base64.decode(object);

        Object obj = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;

        try {
            bis = new ByteArrayInputStream(object);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.LogError(ex);
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ex) {
                    Logger.LogError(ex);
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException ex) {
                    Logger.LogError(ex);
                }
            }
        }
        return obj;
    }
}
