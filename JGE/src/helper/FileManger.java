/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, writeString to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

/**
 * A class to handle FileManger of a project
 *
 * @author Kareem
 */
public class FileManger {

    private File parent;

    /**
     * Set the parent file that the rest of IO will use
     *
     * @param filename The parent file's name
     */
    public FileManger(String filename) {
        parent = FileUtils.getFile(filename);
    }

    /**
     * Get the size of the file and all it possible subfolders
     *
     * @return The size of the file and the possible subfolders. In bytes
     */
    public long size() {
        return size(parent);
    }

    /**
     * Get the size of the file and all it possible subfolders
     *
     * @param f The file to be checked
     * @return The size of the file and the possible subfolders. In bytes
     */
    public long size(File f) {
        long size = f.getTotalSpace();
        return size;
    }

    /**
     * Gets all the subfolders in the current level of the file
     *
     * @param f The file to check
     * @return Array of all subfolders
     */
    public File[] getSubFolders(File f) {
        Queue<File> tocheck = new LinkedBlockingQueue<>(Arrays.asList(f.listFiles()));
        ArrayList<File> folder = new ArrayList<>();
        tocheck.stream().filter((tocheck1) -> (tocheck1.isDirectory())).forEach((tocheck1) -> {
            folder.add(tocheck1);
        });
        return folder.toArray(new File[folder.size()]);
    }

    /**
     * Gets all the subfolders based on the parent
     *
     * @return Array of all the subfolders
     */
    public File[] getSubFolder() {
        return getSubFolders(parent);
    }

    /**
     * Get all the subfiles on the parent (defined at the constructor) including
     * all subfolders in the current layer
     *
     * @return Array of subfiles
     */
    public File[] getSubFiles() {
        return getSubfiles(parent);
    }

    /**
     * Get all the subfiles on the file specified including all subfolders in
     * the current layer
     *
     * @param f The file to check on
     * @return Array of subfiles
     */
    public File[] getSubfiles(File f) {
        return f.listFiles();
    }

    /**
     * Get any files that fill the requirement of matching of the extension
     * based on the parent file (defined at the constructor)
     *
     * @param extension The extension wanted
     * @return Array of all the items that fulfill the extension
     * @throws helper.FileManger.ExtensionException Throws if it is not a valid
     * extension
     */
    public File[] getExtension(String extension) throws ExtensionException {
        return getExtension(parent, extension);
    }

    /**
     * Get any files that fill the requirement of matching of the extension
     *
     * @param f The folder to check
     * @param extension The extension wanted
     * @return Array of all the items that fulfill the extension
     * @throws helper.FileManger.ExtensionException Throws if it is not a valid
     */
    public File[] getExtension(File f, String extension) throws ExtensionException {
        if (extension.charAt(0) != '.') {
            throw new ExtensionException("Extension does not start with a .");
        }
        Queue<File> tocheck = new LinkedBlockingQueue<>(Arrays.asList(f.listFiles()));
        ArrayList<File> found = new ArrayList<>();
        while (!tocheck.isEmpty()) {
            File current = tocheck.poll();
            Arrays.asList(current.listFiles()).forEach((File e) -> {
                if (e.isDirectory()) {
                    //Do nothing
                } else if (FileFilterUtils.suffixFileFilter(extension).accept(e)) {
                    found.add(e);
                }
            });
        }

        return found.toArray(new File[found.size()]);
    }

    /**
     * Get every file that matches the extension in every subfolder. Based on
     * the parent (defined in the constructor)
     *
     * @param extension The exetension wanted
     * @return Array of every file that matches the extension
     * @throws helper.FileManger.ExtensionException Throws this if the extension is
     * invalid
     */
    public File[] getAllExtension(String extension) throws ExtensionException {
        return getAllExtension(parent, extension);
    }

    /**
     * Get every file that matches the extension in every subfolder.
     *
     * @param f The file to been checked
     * @param extension The exetension wanted
     * @return Array of every file that matches the extension
     * @throws helper.FileManger.ExtensionException Throws this if the extension is
     * invalid
     */
    public File[] getAllExtension(File f, String extension) throws ExtensionException {
        if (extension.charAt(0) != '.') {
            throw new ExtensionException("Extension does not start with a .");
        }

        Queue<File> tocheck = new LinkedBlockingQueue<>(Arrays.asList(f.listFiles()));
        ArrayList<File> found = new ArrayList<>();
        while (!tocheck.isEmpty()) {
            File current = tocheck.poll();
            Arrays.asList(current.listFiles()).forEach((File e) -> {
                if (e.isDirectory()) {
                    tocheck.offer(e);
                } else if (FileFilterUtils.suffixFileFilter(extension).accept(e)) {
                    found.add(e);
                }
            });
        }

        return found.toArray(new File[found.size()]);

    }

    /**
     * Will be thrown if the extension given is incorrect
     */
    public static class ExtensionException extends Exception {

        public ExtensionException(String message) {
            super(message);
        }

    }
}
