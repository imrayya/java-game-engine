package global.events;

import java.awt.event.MouseEvent;
import java.util.LinkedList;

/**
 * A mouse listener
 * <p>
 * Inbetween the UI and the engine
 * <p>
 * Not thread safe yet currently
 *
 * @author Kareem
 */
public class MouseListener extends Observerable {

    /**
     * All the mouse event
     */
    private static final LinkedList<MouseEvent> MOUSE_EVENT = new LinkedList<>();
    /**
     * The instance of the mouseListener
     */
    private static final MouseListener SINGLETON = new MouseListener();

    /**
     * Private constructor for the singleton
     */
    private MouseListener() {

    }

    /**
     * Get the instance of the Mouse Listener
     *
     * @return The instance
     */
    public static MouseListener getInstance() {
        return SINGLETON;
    }

    /**
     * Adds a new mouse instance
     *
     * @param e The mouse event
     */
    public void addItem(MouseEvent e) {
        MOUSE_EVENT.push(e);
        notifyObservers();
    }

    /**
     * Get the mouse instance
     *
     * @return The mouse event
     */
    public MouseEvent getItem() {
        return MOUSE_EVENT.pop();
    }
}
