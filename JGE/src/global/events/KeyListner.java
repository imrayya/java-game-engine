package global.events;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * A key listener
 * <p>
 * Inbetween the UI and the engine
 * <p>
 * Not thread safe yet currently
 *
 * @author Kareem
 */
public class KeyListner extends Observerable {

    /**
     * Current key presses
     */
    private final ArrayList<Integer> CURRENT_KEY_PRESS = new ArrayList<>();

    /**
     * The instance of the keyListener
     */
    private static final KeyListner SINGLETON = new KeyListner();

    private KeyListner() {
    }

    public static KeyListner getInstance() {
        return SINGLETON;
    }

    /**
     * Adds a new item to what has been press
     *
     * @param e The keyevent that needs to be added
     */
    public void addItem(KeyEvent e) {
        if (!CURRENT_KEY_PRESS.contains(e.getKeyCode())) {
            CURRENT_KEY_PRESS.add(e.getKeyCode());
            notifyObservers();
        }
    }

    /**
     * Removes an item to what has been pressed (the user released the key)
     *
     * @param e The Keyevent to be removed
     */
    public void removeItem(KeyEvent e) {
        CURRENT_KEY_PRESS.remove(new Integer(e.getKeyCode()));
        notifyObservers();
    }

    /**
     * Returns all the current keys that is currently been pushed
     *
     * @return An array of keycodes of the current keys being pressed
     */
    public Integer[] getItems() {
        return CURRENT_KEY_PRESS.toArray(new Integer[CURRENT_KEY_PRESS.size()]);
    }

    /**
     * Clears what is currently been pressed
     *
     * @return whether this was successfully done
     */
    public boolean clear() {
        CURRENT_KEY_PRESS.clear();
        return true;
    }

}
