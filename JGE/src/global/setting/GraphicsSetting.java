package global.setting;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/**
 * Graphical setting to be used in the rest of the game
 *
 * @author Kareem
 */
public class GraphicsSetting {

    /**
     * The screen size to be used
     */
    public static Rectangle2D screenSize = new Rectangle(0, 0, 1000, 1000);
    /**
     * The target FPS
     */
    public static int FPS;

    /**
     * Gets the width of the screen
     *
     * @return The screen size in pixels
     */
    public static int getWidth() {
        return (int) screenSize.getWidth();
    }

    /**
     * Gets the height of the screen
     *
     * @return The screen size in pixels
     */
    public static int getHeight() {
        return (int) screenSize.getWidth();
    }

    /**
     * The target FPS
     *
     * @return The FPS
     */
    public static int maxFPS() {
        return FPS;
    }
}
