package start;

import helper.Logger;
import game.Start;

/**
 * Start file for the rest of the program
 *
 * @author Kareem
 */
public class StartFile {

    /**
     * Creates a new thread to start the game
     *
     * @param args - Does nothing so far
     */
    public static void main(String[] args) {
        try {
            new Thread(new Start()).start();
        } catch (Exception e) {
            Logger.LogError(e.toString(), StartFile.class.getName());
            Logger.exit();
        }
    }

}
