package game;

import global.events.Observer;
import global.events.Observerable;
import global.setting.GraphicsSetting;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A ticker to say that object and game logic should run
 *
 * @author Kareem
 */
public class Ticker extends Observerable implements Runnable {

    /**
     * Creates a tick
     */
    @Override
    public void run() {
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                notifyObservers("tick");
                SingletonHolder.checkDeath();

            }
        }, 0, 1000 / GraphicsSetting.FPS
        );
    }

}
