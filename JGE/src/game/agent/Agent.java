/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package game.agent;

import game.GameObject;
import game.SingletonHolder;
import game.projectle.Projectle;
import java.awt.Rectangle;

/**
 * A general agent class. Meant to allow an game object to move and have health
 *
 * @author Kareem
 */
public abstract class Agent extends GameObject {
    
    protected int rateOfMovement = 1;
    protected long maxHealth = 2;
    protected long currentHealth = 2;
    /**
     * The touch box
     */
    protected Rectangle hitBox;
    
    public Agent() {
        super();
        SingletonHolder.agents.add(this);
    }

    /**
     * Check if two agent touch each other
     *
     * @param a The agent in question
     * @return Returns true if the agent does intersect the object
     */
    public boolean touch(Projectle a) {
        return a.getHitBox().intersects(hitBox);
    }

    /**
     * Damages the agent
     *
     * @param pain How much damage it get inflicted
     */
    public void getHurt(long pain) {
        currentHealth -= pain;
    }
    
}
