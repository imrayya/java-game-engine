package game;

import ui.graphic.Renderable;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import ui.graphic.BridgeDoubleBuffer;

/**
 * A game object to be used by the rest of the game. Encapsulate Renderable
 *
 * @see Renderable
 *
 * @author Kareem
 */
public abstract class GameObject extends Renderable {

    private boolean alive = true;

    /**
     * A default constructor
     */
    public GameObject() {
        super();
        BridgeDoubleBuffer.getInstance().add(this);
        SingletonHolder.objects.add(this);
    }

    /**
     * Creates a new game object with the desired image and location
     *
     * @param i
     * @param key
     * @param location
     */
    public GameObject(BufferedImage i, String key, Point2D location) {
        super(i, key, location);
        BridgeDoubleBuffer.getInstance().add(this);
    }

    /**
     * Creates a new game object with the desired image and location
     *
     * @param i
     * @param location
     */
    public GameObject(String i, Point2D location) {
        super(i, location);
        BridgeDoubleBuffer.getInstance().add(this);
    }

    /**
     * Tells the engine to kill this object
     */
    protected void kill() {
        alive = false;
    }

    /**
     * Say if this object is alive. If not, the object will be deleted
     *
     * @return if false, signals to delete the object
     */
    public boolean isAlive() {
        return alive;
    }

}
