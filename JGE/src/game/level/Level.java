/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package game.level;

import java.awt.Rectangle;
import java.io.Serializable;
import ui.graphic.Renderable;

/**
 *
 * @author Kareem
 */

public abstract class Level implements Serializable{

    protected Renderable renderAble;
    protected Rectangle[] blocking;

    public Level() {
    }

    /**
     * What type of block (cell) is this location
     *
     * @param x The x location in question
     * @param y The y location in question
     * @return What type of block it is
     */
    public abstract int topOfQuestionMark(int x, int y);

    /**
     * If a player or something is allowed on the cell
     *
     * @param r The agent's hitbox
     * @return If it is allowed to touch the thingy
     */
    public abstract boolean allowed(Rectangle r);

    /**
     * Transform an int[][] to a renderable
     */
    public abstract void create();

}
