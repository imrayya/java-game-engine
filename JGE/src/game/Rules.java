package game;

import game.agent.Agent;
import game.top_down.agent.Player;
import helper.Logger;
import game.top_down.level.Level;

/**
 * The rules of the game
 *
 * @author Kareem
 */
public class Rules {

    /**
     * Checks if a player has won
     *
     * @param p The player
     */
    public static void checkWin(Player p) {
        if (SingletonHolder.level.topOfQuestionMark(p.getX(), p.getY()) == 4) {
            Logger.LogHigh("WIN");
            for (Agent agent : SingletonHolder.agents) {
                agent.setX(50);
                agent.setY(50);
                
            }
            SingletonHolder.level = new Level(30);
        }
    }
}
