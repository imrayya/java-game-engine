package game;

import game.agent.Agent;
import game.projectle.Projectle;
import game.top_down.level.Level;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import ui.graphic.BridgeDoubleBuffer;

/**
 * A holder of many object that is only made once, should be removed eventually
 *
 * @author Kareem
 * @deprecated
 */
@Deprecated
public class SingletonHolder {

    /**
     * A ticker to tell the internal engine that tick has been made
     */
    public static Ticker tick;

    /**
     * The current level
     */
    public static Level level;

    /**
     * All current game object
     */
    public static Deque<GameObject> objects = new ArrayDeque<>();

    /**
     * All the agents
     */
    public static Deque<Agent> agents = new ArrayDeque<>();

    /**
     * To be moved inside of gameObject
     */
    public static void checkDeath() {
        //TODO
        ArrayList<GameObject> tmp = new ArrayList<>();
        objects.stream().filter((object) -> (!object.isAlive())).forEach((object) -> {
            tmp.add(object);
        });
        tmp.stream().map((tmp1) -> {
            objects.remove(tmp1);
            if (tmp1 instanceof Projectle) {
                tick.removeObserver((Projectle) tmp1);
            }
            return tmp1;
        }).map((tmp1) -> {
            tmp1.setVisible(false);
            return tmp1;
        }).forEach((tmp1) -> {
            BridgeDoubleBuffer.getInstance().remove(tmp1);
        });
    }
}
