/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package game.projectle;

import game.GameObject;
import game.SingletonHolder;
import global.events.Observer;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * A abstract class that all projectiles (non-hitscan) should extend
 *
 * @author Kareem
 */
public abstract class Projectle extends GameObject implements Observer {

    /**
     * A value to show that it should move in particular direction
     */
    public static final int UP = 1;
    /**
     * A value to show that it should move in particular direction
     */
    public static final int DOWN = -1;
    /**
     * A value to show that it should move in particular direction
     */
    public static final int LEFT = 2;
    /**
     * A value to show that it should move in particular direction
     */
    public static final int RIGHT = -2;
    protected int damage;
    protected int rateOfMovement;
    protected int[] direction = new int[]{};
    /**
     * The hit box of the projectile
     */
    protected Rectangle hitBox;

    /**
     * Returns the hit box
     *
     * @return The box that bounds the object
     */
    public Rectangle getHitBox() {
        return hitBox;
    }

    public Projectle() {
        super();
    }

    public Projectle(String i, Point2D location) {
        super(i, location);
    }

    public Projectle(BufferedImage i, String key, Point2D location) {
        super(i, key, location);
    }

    public final void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * Get how much damage this projectile inflicts
     *
     * @return The amount of damage
     */
    public abstract int getDamage();

    @Override
    public void update() {
        if (isAlive()) {
            for (int e : direction) {
                switch (e) {
                    case UP:
                        moveUp();
                        break;
                    case DOWN:
                        moveDown();
                        break;
                    case LEFT:
                        moveLeft();
                        break;
                    case RIGHT:
                        moveRight();
                        break;
                }
            }
        } else {
            SingletonHolder.tick.removeObserver(this);
//            SingletonHolder.checkDeath();
        }
    }

    @Override
    public void update(String args) {
        update();
    }

    public abstract void moveUp();

    public abstract void moveDown();

    public abstract void moveLeft();

    public abstract void moveRight();

}
