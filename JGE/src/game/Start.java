package game;

import game.top_down.agent.KareemAI;
import game.top_down.agent.Player;
import helper.Logger;
import game.top_down.level.Level;
import global.setting.GraphicsSetting;
import ui.graphic.BridgeDoubleBuffer;

/**
 * Starts the actual game
 *
 * @author Kareem
 */
public class Start implements Runnable {

    /**
     * Creates all the necessary object and runs the game
     */
    @Override
    public void run() {
        GraphicsSetting.FPS = 60;
        try {
            SingletonHolder.level = new Level(30, 30);
            Thread ui = new Thread(BridgeDoubleBuffer.getInstance());
            SingletonHolder.tick = new Ticker();
            Thread ticker = new Thread(SingletonHolder.tick);
            Player player = new Player();
            KareemAI ai = new KareemAI();
            ticker.start();
            ui.start();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.LogError(e.toString(), Start.class.getName());
            Logger.exit();
        }

    }

}
