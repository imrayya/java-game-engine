package game.top_down.agent;

import game.Rules;
import game.SingletonHolder;
import game.top_down.projectle.Bolt;
import game.top_down.projectle.Projectle;
import global.events.KeyListner;
import global.events.Observer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * The human player
 *
 * @author Kareem
 */
public class Player extends Agent implements Observer {

    /**
     * Creates a new player that is a red square
     */
    public Player() {
        super();
        BufferedImage a = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
        Graphics g = a.createGraphics();
        g.setColor(Color.red);
        g.fillRect(0, 0, 10, 10);
        g.dispose();
        addImage(a,"Player");

        setLocation(new Point2D.Double(50, 50));
        this.setPriority(100);
        this.setVisible(true);
        SingletonHolder.tick.addObserver(this);
        rateOfMovement = 2;
    }

    @Override
    public void update() {

    }

    @Override
    public void update(String args) {
        move(KeyListner.getInstance().getItems());
    }

    /**
     * Move the player
     * <p>
     * TODO remove the shooting mechanic of the player to another method
     *
     * @param keyCodes The key from the
     */
    private void move(Integer[] keyCodes) {
        for (Integer keyCode : keyCodes) {
            int a = keyCode;
            switch (a) {
                case KeyEvent.VK_W:
                    moveUp();
                    break;
                case KeyEvent.VK_S:
                    moveDown();
                    break;
                case KeyEvent.VK_A:
                    moveRight();
                    break;
                case KeyEvent.VK_D:
                    moveLeft();
                    break;
                case KeyEvent.VK_DOWN:
                    new Bolt(new int[]{Projectle.DOWN}, getX(), getY());
                    break;
                case KeyEvent.VK_UP:
                    new Bolt(new int[]{Projectle.UP}, getX(), getY());
                    break;
                case KeyEvent.VK_LEFT:
                    new Bolt(new int[]{Projectle.LEFT}, getX(), getY());
                    break;
                case KeyEvent.VK_RIGHT:
                    new Bolt(new int[]{Projectle.RIGHT}, getX(), getY());
                    break;
            }
        }
        Rules.checkWin(this);
    }

}
