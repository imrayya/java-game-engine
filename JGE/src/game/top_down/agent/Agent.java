package game.top_down.agent;

import game.SingletonHolder;
import java.awt.Rectangle;

/**
 * A general agent class for a top down game. Meant to allow an game object to
 * move and have health
 *
 * @author Kareem
 */
public class Agent extends game.agent.Agent {

    /**
     * Default constructor
     */
    public Agent() {
        super();
    }

    /**
     * Signifies that the agent should move upwards
     */
    public void moveUp() {
        setY(getY() - rateOfMovement);
        hitBox = new Rectangle(this.getX(), this.getY(), this.getImage().getWidth(), this.getImage().getHeight());
        if (!SingletonHolder.level.allowed(hitBox)) {
            moveDown();
        }
    }

    /**
     * Signifies that the agent should move downwards
     */
    public void moveDown() {
        setY(getY() + rateOfMovement);
        hitBox = new Rectangle(this.getX(), this.getY(), this.getImage().getWidth(), this.getImage().getHeight());
        if (!SingletonHolder.level.allowed(hitBox)) {
            moveUp();
        }
    }

    /**
     * Signifies that the agent should move to the left
     */
    public void moveLeft() {
        setX(getX() + rateOfMovement);
        hitBox = new Rectangle(this.getX(), this.getY(), this.getImage().getWidth(), this.getImage().getHeight());
        if (!SingletonHolder.level.allowed(hitBox)) {
            moveRight();
        }
    }

    /**
     * Signifies that the agent should move to the right
     */
    public void moveRight() {
        setX(getX() - rateOfMovement);
        hitBox = new Rectangle(this.getX(), this.getY(), this.getImage().getWidth(), this.getImage().getHeight());
        if (!SingletonHolder.level.allowed(hitBox)) {
            moveLeft();
        }
    }
}
