/*
 * Copyright (C) 2016 Kareem
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package game.top_down.agent;

import game.SingletonHolder;
import game.top_down.level.MatrixGraph;
import global.events.Observer;
import helper.Logger;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Kareem
 */
public class KareemAI extends Agent implements Observer {

    MatrixGraph g;

    public KareemAI() {
        super();
        BufferedImage a = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
        Graphics g = a.createGraphics();
        g.setColor(Color.blue);
        g.fillRect(0, 0, 10, 10);
        g.dispose();
        addImage(a, "KareemAI");

        setLocation(new Point2D.Double(55, 55));
        this.setPriority(100);
        this.setVisible(true);
        SingletonHolder.tick.addObserver(this);
        rateOfMovement = 2;

        this.g = SingletonHolder.level.getMatrixGraph();
    }

    @Override
    public void update() {
        if(!isAlive()){
            SingletonHolder.tick.removeObserver(this);
        }
        checkHealth();
        moveDown();        
    }

    @Override
    public void update(String args) {
        update();
    }
    
    public void checkHealth(){
        if(currentHealth <=0){
            kill();
        }
    }

}
