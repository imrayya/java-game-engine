package game.top_down.projectle;

import game.GameObject;
import game.SingletonHolder;
import game.top_down.agent.Agent;
import java.awt.geom.Point2D;

/**
 * A abstract class that all projectiles (non-hitscan) should extend for top
 * down
 *
 * @author Kareem
 */
public abstract class Projectle extends game.projectle.Projectle {

    /**
     * Default constructor when the projectile shouldn't move.
     */
    public Projectle() {
        super();
        this.rateOfMovement = 3;
        this.damage = 0;
        SingletonHolder.tick.addObserver(this);
    }

    /**
     * For a projectile that is moving particular direction. Direction is the
     * current movement
     *
     * @param direction The direction that it will move in
     */
    public Projectle(int[] direction) {
        this();
        this.rateOfMovement = 3;
        this.damage = 0;
        this.direction = direction;
    }

    /**
     * For a projectile that is moving particular direction
     *
     * @param direction The direction that it will move in
     * @param i What image it should have
     * @param location Where it currently is
     */
    public Projectle(int[] direction, String i, Point2D location) {
        super(i, location);
        this.rateOfMovement = 3;
        this.damage = 0;
    }

    /**
     * Get how much damage this projectile inflicts
     *
     * @return The amount of damage
     */
    @Override
    public int getDamage() {
        return damage;
    }

    public void checkDamage() {
        SingletonHolder.objects.stream().filter((object) -> 
                (object instanceof Agent && ((Agent)object).touch(this))).
                forEach((object) -> {
            ((Agent) object).getHurt(this.damage);
        });
    }

    @Override
    public void moveUp() {
        setY(getY() - rateOfMovement);
        hitBox.setLocation(this.getX(), this.getY());
        if (!SingletonHolder.level.allowed(hitBox)) {
            kill();
        }
        checkDamage();
    }

    @Override
    public void moveDown() {
        setY(getY() + rateOfMovement);
        hitBox.setLocation(this.getX(), this.getY());
        if (!SingletonHolder.level.allowed(hitBox)) {
            kill();
        }
        checkDamage();

    }

    @Override
    public void moveLeft() {
        setX(getX() - rateOfMovement);
        hitBox.setLocation(this.getX(), this.getY());
        if (!SingletonHolder.level.allowed(hitBox)) {
            kill();
        }
        checkDamage();

    }

    @Override
    public void moveRight() {
        setX(getX() + rateOfMovement);
        hitBox.setLocation(this.getX(), this.getY());
        if (!SingletonHolder.level.allowed(hitBox)) {
            kill();
        }
        checkDamage();

    }

}
