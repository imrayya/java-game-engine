package game.top_down.projectle;

import game.SingletonHolder;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 * A specific type of projectile. A simple bolt
 *
 * @author Kareem
 */
public class Bolt extends Projectle {

    /**
     * For a projectile that is moving particular direction
     *
     * @param direction The direction that it will move in
     * @param x X-location
     * @param y Y-location
     */
    public Bolt(int[] direction, int x, int y) {
        super(direction);
        BufferedImage a = null;
        addImage("bullet2");
        this.resize(0.1);
        for (int e : direction) {
            switch (e) {
                case UP:
                    rotate(0.5*Math.PI);
                    break;
                case DOWN:
                    rotate(-0.5*Math.PI);
                    break;
                case RIGHT:
                    rotate(Math.PI);
                    break;
            }
        }
        damage = 1;
        hitBox = new Rectangle(x, y, getImage().getWidth(), getImage().getHeight());
        setLocation(new Point2D.Double(x, y));
        this.setPriority(50);
        this.setVisible(true);
        SingletonHolder.tick.addObserver(this);
    }

}
