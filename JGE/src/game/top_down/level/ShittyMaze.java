package game.top_down.level;

import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Generates a shitty maze. Based on every square, its either blocked off or
 * not. Does a BFS to see if it is a viable maze
 * <p>
 * PLEASE REWRITE ME OR REPLACE ME, IM IN SO MUCH PAIN
 *
 * @author Kareem
 */
public class ShittyMaze {

    private final int x;
    private final int y;
    private final int[][] maze;
    private Random r = new Random();

    /**
     * Creates the outline of the shitty maze
     *
     * @param x How wide you want it
     * @param y How high you want it
     */
    public ShittyMaze(int x, int y) {
        this.x = x;
        this.y = y;
        maze = new int[this.x][this.y];
    }

    /**
     * Generates the shitty maze. I'm so sorry
     *
     * @param startx Where you start on
     * @param starty Where you start on
     * @param endx Where the end location
     * @param endy Where the end location is
     * @return The maze
     */
    public int[][] generateMaze(int startx, int starty, int endx, int endy) {
        do {
            for (int i = 0; i < x; i++) {
                for (int j = 0; j < y; j++) {
                    if (r.nextDouble() > 0.4) {
                        maze[i][j] = 0;
                    } else {
                        maze[i][j] = 1;
                    }
                    if (i == 0 || j == 0 || i == x - 1 || j == y - 1) {
                        maze[i][j] = 1;
                    }
                }
            }
        } while (!bfs(startx, starty, endx, endy));
        maze[startx][starty] = 3;
        maze[endx][endy] = 4;
        return maze;
    }

    /**
     * See what cell is connected to what cell, no diagonal
     *
     * @param a The location of cell to check
     * @return All the location of every other cell that is connected to 'a'
     */
    private int[][] connecter(int[] a) {
        ArrayList<int[]> b = new ArrayList<>();
        if (maze[a[0] + 1][a[1]] == 0) {
            b.add(new int[]{a[0] + 1, a[1]});
        }

        if (maze[a[0]][a[1] + 1] == 0) {
            b.add(new int[]{a[0], a[1] + 1});

        }

        if (maze[a[0] - 1][a[1]] == 0) {
            b.add(new int[]{a[0] - 1, a[1]});

        }

        if (maze[a[0]][a[1] - 1] == 0) {
            b.add(new int[]{a[0], a[1] - 1});

        }
        return b.toArray(new int[b.size()][2]);
    }

    /**
     * A breath first search to see if its a viable maze
     *
     * @param startx Where you start on
     * @param starty Where you start on
     * @param endx Where the end location
     * @param endy Where the end location is
     * @return If it is possible to go from the start to the end
     */
    private boolean bfs(int startx, int starty, int endx, int endy) {
        if (maze[startx][starty] == 1) {
            return false;
        }
        ArrayList<int[]> alreadyChecked = new ArrayList<>();
        Queue<int[]> toDo1 = new LinkedBlockingQueue<>();
        alreadyChecked.add(new int[]{1, 1});
        toDo1.offer(new int[]{startx, starty});
        do {
            int[] currentNode = toDo1.poll();
            //Gets what this node is connected to
            int[][] line = connecter(currentNode);
            for (int j = 0; j < line.length; j++) {
                //same check as in DF
                if (maze[line[j][0]][line[j][1]] == 0 && checked(alreadyChecked, line[j])) {
                    //We need to check this in the next round
                    toDo1.offer(line[j]);
                    //Addes it to something we already checked
                    alreadyChecked.add(line[j]);

                }
            }
        } while (!toDo1.isEmpty());
        return !checked(alreadyChecked, new int[]{endx, endy});
    }

    /**
     * ????
     *
     * @param a ?
     * @param tobechecked ?
     * @return ?
     */
    private boolean checked(ArrayList<int[]> a, int[] tobechecked) {
        return a.stream().noneMatch((a1) -> (tobechecked[0] == a1[0] && tobechecked[1] == a1[1]));
    }
}
