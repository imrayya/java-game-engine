package game.top_down.level;

import ui.graphic.BufferedImageHelper;
import ui.graphic.Renderable;
import helper.Logger;
import global.setting.GraphicsSetting;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import ui.graphic.BridgeDoubleBuffer;

/**
 * The current level
 *
 * @author Kareem
 */
public class Level extends game.level.Level {

    private int[][] level;
    private BufferedImage levelImage;
    private MatrixGraph node;

    /**
     * Gets a matrix graph
     *
     * @return The matrix graph
     */
    public MatrixGraph getMatrixGraph() {
        return node;
    }

    /**
     * What type of block (cell) is this location
     *
     * @param x The x location in question
     * @param y The y location in question
     * @return What type of block it is
     */
    public int topOfQuestionMark(int x, int y) {
        int width = GraphicsSetting.getWidth() / level.length;
        int height = GraphicsSetting.getWidth() / level[0].length;
        return level[x / width][y / height];
    }

    /**
     * Creates a level based on a square
     *
     * @param size The size wanted
     */
    public Level(int size) {
        level = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                level[i][j] = 1;
            }
        }
        for (int i = 0; i < size; i++) {
            level[i][size - 1] = 2;
            level[size - 1][i] = 2;
            level[i][0] = 2;
            level[0][i] = 2;
        }
        createMaze();
        create();
    }

    /**
     * Creates a new level based on the parameters
     *
     * @param x The width
     * @param y The height
     */
    public Level(int x, int y) {
        level = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                level[i][j] = 1;
                if (i == 0 || j == 0 || i == x - 1 || j == y - 1) {
                    level[i][j] = 1;
                }
            }
        }
        createMaze();
        create();
    }

    /**
     * Creates a maze, a place holder
     */
    private void createMaze() {
        int startx = 1;
        int starty = 1;

        int endx = level.length - 4;
        int endy = level[0].length - 4;

        level = new ShittyMaze(level.length, level[0].length).generateMaze(startx, starty, endx, endy);
        node = new MatrixGraph(level);
    }

    /**
     * If a player or something is allowed on the cell
     *
     * @param r The agent's hitbox
     * @return If it is allowed to touch the thingy
     */
    public boolean allowed(Rectangle r) {
        for (Rectangle a : blocking) {
            if (a.intersects(r)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Creates a new level
     *
     * @param level What the new level is
     */
    public void setLevel(int[][] level) {
        this.level = level;
        create();
    }

    /**
     * Transform an int[][] to a renderable
     */
    public final void create() {
        int width = (int) GraphicsSetting.screenSize.getWidth() / level.length;
        int height = (int) GraphicsSetting.screenSize.getHeight() / level[0].length;
        BufferedImage image = new BufferedImage(width * level.length, height * level[0].length, BufferedImage.TYPE_INT_ARGB);
        BufferedImage walk = null;
        BufferedImage block = null;
        BufferedImage goal = null;
        BufferedImage target = null;
        try {
            walk = ImageIO.read(new File("gameresources\\ground\\Grasstop.png"));
            block = ImageIO.read(new File("gameresources\\ground\\Stone.png"));
            goal = ImageIO.read(new File("gameresources\\ground\\Diamond.png"));
            target = ImageIO.read(new File("gameresources\\ground\\Dirtbottom.png"));
        } catch (IOException ex) {
            Logger.LogError(ex.toString(), this);
        }
        walk = BufferedImageHelper.resize(walk, width, height);
        block = BufferedImageHelper.resize(block, width, height);
        goal = BufferedImageHelper.resize(goal, width, height);
        target = BufferedImageHelper.resize(target, width, height);
        Graphics g = image.getGraphics();
        ArrayList<Rectangle> r = new ArrayList<>();
        for (int i = 0; i < level.length; i++) {
            for (int j = 0; j < level[i].length; j++) {
                if (level[i][j] == 1) {
                    r.add(new Rectangle(i * width, j * height, width, height));
                    g.drawImage(block, i * width, j * height, null);
                } else if (level[i][j] == 3) {
                    g.drawImage(target, i * width, j * height, null);
                } else if (level[i][j] == 4) {
                    g.drawImage(goal, i * width, j * height, null);
                } else {
                    g.drawImage(walk, i * width, j * height, null);
                }
            }
        }
        g.dispose();
        levelImage = image;
        blocking = r.toArray(new Rectangle[r.size()]);
        if (renderAble == null) {
            renderAble = new Renderable(image, "level", new Point2D.Double());
        } else {
            renderAble.replace(levelImage);
        }
        BridgeDoubleBuffer.getInstance().add(renderAble);
    }

}
