package game.top_down.level;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A graph with an Adjacency Matrix for the back end
 * 
 * <p>
 * Meant to be check if a level is allowed
 *
 * @author Kareem
 */
public class MatrixGraph {

    //Adjacency Matrix
    private final int[][] MATRIX;
    //Contains all the nodes
    private final int[] NODES;
    //Current size of the 
    private int index = 0;
    //If the graph is directed or not
    private final boolean DIRECTED;

    private HashMap<IntegerArrayEncap, Integer> map;

    /**
     * Sets the capacity of the graph (How many nodes are allowed). Undirected
     * graph
     *
     * @param numberOfNodes The number of nodes wanted
     */
    public MatrixGraph(int numberOfNodes) {
        this.MATRIX = new int[numberOfNodes][numberOfNodes];
        this.NODES = new int[numberOfNodes];
        DIRECTED = false;
    }

    /**
     * Makes Matrix graph based on an array for a level
     *
     * @param level An 2D array that represent a matrix graph
     */
    public MatrixGraph(int[][] level) {
        int numberOfNodes = level.length * level[0].length;
        this.MATRIX = new int[numberOfNodes][numberOfNodes];
        this.NODES = new int[numberOfNodes];
        DIRECTED = false;
        map = new HashMap<>();
        int c = 0;
        for (int i = 0; i < level.length; i++) {
            for (int j = 0; j < level[0].length; j++) {
                addNode(level[i][j]);
                map.put(new IntegerArrayEncap(new int[]{i, j}), c++);
            }
        }
        for (int i = 0; i < level.length; i++) {
            for (int j = 0; j < level[0].length; j++) {
                int[][] connected = connecter(new int[]{i, j}, level);
                if (connected != new int[0][0]) {
                    for (int[] connected1 : connected) {
                        connect(map.get(new IntegerArrayEncap(connected1)), map.get(new IntegerArrayEncap(new int[]{i, j})));
                    }
                }
            }
        }
    }

    /**
     * Sets the capacity of the graph (How many nodes are allowed).
     *
     * @param numberOfNodes The number of nodes wanted
     * @param directed Boolean to set if the graph should be directed or not
     */
    public MatrixGraph(int numberOfNodes, boolean directed) {
        this.MATRIX = new int[numberOfNodes][numberOfNodes];
        this.NODES = new int[numberOfNodes];
        this.DIRECTED = directed;
    }

    /**
     * Honestly, I forgot what this does TODO
     *
     * @param a ?
     * @param array ?
     * @return ?
     */
    private int[][] connecter(int[] a, int[][] array) {
        if (array[a[0]][a[1]] == 1) {
            return new int[0][0];
        }
        ArrayList<int[]> b = new ArrayList<>();
        if (array[a[0] + 1][a[1]] == 0) {
            b.add(new int[]{a[0] + 1, a[1]});
        }

        if (array[a[0]][a[1] + 1] == 0) {
            b.add(new int[]{a[0], a[1] + 1});

        }

        if (array[a[0] - 1][a[1]] == 0) {
            b.add(new int[]{a[0] - 1, a[1]});

        }

        if (array[a[0]][a[1] - 1] == 0) {
            b.add(new int[]{a[0], a[1] - 1});

        }
        return b.toArray(new int[b.size()][2]);
    }

    /**
     * Adds a new node
     *
     * @param blockType The block that wants to be inputted
     */
    public void addNode(int blockType) {
        //Checks if there still is capicity for a new node
        if (NODES.length <= index) {
            System.out.println("Error");
        } else {
            NODES[index] = blockType;
            index++;
        }
    }

    /**
     * Connects 2 nodes based on the time it entered with no weights
     *
     * @param node1 The first index
     * @param node2 The second index
     */
    public void connect(int node1, int node2) {
        //Checks if the index is not out of bounds
        if (NODES.length <= node1 || NODES.length <= node2) {
            System.out.println("Error, index out of bound");
        } else {
            MATRIX[node1][node2] = 1;
            if (!DIRECTED) {
                MATRIX[node2][node1] = 1;
            }
        }
    }

    /**
     * Connects 2 nodes based on the time it is entered with weights included
     *
     * @param node1 The index of the first node
     * @param node2 The index of the second node
     * @param weight The weight of the connection
     */
    public void connect(int node1, int node2, int weight) {
        //Checks if the index is not out of bounds
        if (NODES.length <= node1 || NODES.length <= node2) {
            System.out.println("Error, index out of bound");
        } else {
            MATRIX[node1][node2] = weight;
            if (!DIRECTED) {
                MATRIX[node2][node1] = weight;
            }
        }
    }

    /**
     * Prints the Graph
     */
    public void print() {
        System.out.print("Node: ");
        for (Object node : NODES) {
            System.out.print(node.toString() + " ");
        }

        System.out.print('\n');
        System.out.println("Matrix:");
        for (int[] matrix1 : MATRIX) {
            for (int i = 0; i < matrix1.length; i++) {
                System.out.print(matrix1[i]);
            }
            System.out.print('\n');
        }
        System.out.println("");
        System.out.println("");
    }

    /**
     * Gets the size of the graph
     *
     * @return The size of the graph
     */
    public int size() {
        return NODES.length;
    }

    /**
     * An encapsulation of a int array []{a,b}
     */
    protected static class IntegerArrayEncap {

        int[] a;

        public IntegerArrayEncap(int[] a) {
            this.a = a;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof IntegerArrayEncap)) {
                System.out.println("fail");
                return false;
            }
            IntegerArrayEncap b = (IntegerArrayEncap) obj;
            return (a[0] == b.a[0] && a[1] == b.a[1]);
        }

        @Override
        public int hashCode() {
            int hash = 69;
            hash = hash * 31 + a[0];
            hash = hash * 31 + a[1];
            return hash;
        }
    }

}
